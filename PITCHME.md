# Git tutorial

## Part 0: Introduction


---
@snap[east]
<img src="https://git-scm.com/images/logos/downloads/Git-Icon-1788C.png" width="200">
@snapend

@quote[`git` /ɡɪt/ is a version-control system for tracking changes in computer files and coordinating work on those files among multiple people.](Wikipedia)

---

What is version control:

@ul
* `manuscript.tex`
* `manuscript_v2.tex`
* `manuscript_v3.tex`
* `manuscript_final.tex`
* `manuscript_final_v2.tex`
* `manuscript_final_i_swear_this_is_it.tex`
@ulend

---?image=http://blog.netsons.com/wp-content/uploads/2016/11/basic-remote-workflow.png&size=auto 90%

---

## Part 1: Local workflow

---
## Creating a repository

We create a folder for our repository

```console
$ mkdir git_tutorial

$ cd git_tutorial
```

and we **initialize** it

```console
$ git init
Initialized empty Git repository in ~/git_tutorial/.git/
```

---
Our directory is now a git repository. There is a hidden folder `.git`, where git stores all its information.

```console
$ ls -la
total 8
drwxr-xr-x 1 teore 197609 0 Oct  8 14:10 ./
drwxr-xr-x 1 teore 197609 0 Oct  8 14:11 ../
drwxr-xr-x 1 teore 197609 0 Oct  8 14:10 .git/
```
@[5](This folder contains all the repository information)

> Everything is local, nothing is saved elsewhere on the computer.

---

## Status of the repository

```console
$ git status
On branch master

No commits yet

nothing to commit (create/copy files and use "git add" to track)
```

---
## Adding files
Now we create a `hello.txt` file with the text

```text
Hello World!
```

> @fa[windows] Windows users add a final empty line for cosmetic reasons.

---

Let's check the status

```console
$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        hello.txt

nothing added to commit but untracked files present (use "git add" to track)
```

---

## Creating a commit

We first add the changed file to the staging area

```console
$ git add hello.txt
```

> ```console
> $ git add .
> ```
> adds all the files in the directory


---

We create the commit. But an error appears

```console
$ git commit

*** Please tell me who you are.

Run

  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"

to set your account's default identity.
Omit --global to set the identity only in this repository.

fatal: unable to auto-detect email address (got 'SOMETHING')
```

Do as git says (you can omit `--global`).

---

Now let's try again

```console
$ git commit
```

A text editor will appear. Let's write `Initial commit`.

```console
[master (root-commit) 0e19cef] Initial commit
 1 file changed, 1 insertion(+)
 create mode 100644 hello.txt
```

Git tells the branch, the ID of the commit, the message, and the files involved.

---

## Committing changes
Let's edit `hello.txt` and add

```text
Hello World!

My name is YOURNAME
```

> @fa[windows] Windows users remember the empty line!
---
Git will detect the change


```console
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   hello.txt

no changes added to commit (use "git add" and/or "git commit -a")
```

---
## `git diff`
This command shows the differences between the current workspace and the last commit

```diff
$ git diff
diff --git a/hello.txt b/hello.txt
index 980a0d5..980c625 100644
--- a/hello.txt
+++ b/hello.txt
@@ -1 +1,3 @@
 Hello World!
+
+My name is Matteo.
```


---
We now commit the change

```console
$ git add .

$ git commit -m "Name added"
[master d6615e3] Name added
 1 file changed, 3 insertions(+), 1 deletion(-)
```

> The `-m` option allows to directly write the message.
---

## `git log`
This command shows the history of commits
```console
$ git log
commit d6615e3763721dcfb702d7cd3964124b2e5136e2 (HEAD -> master)
Author: Matteo Rossi <whatever.email@gmail.com>
Date:   Mon Oct 8 15:10:14 2018 +0300

    Name added

commit 0e19cef9f101d65de435a1ae6dc6bd66675901d0
Author: Matteo Rossi <whatever.email@gmail.com>
Date:   Mon Oct 8 14:18:37 2018 +0300

    Initial commit
```

---
## Excercise

Add your surname, check the difference and commit.

Workflow:

* Make changes
* Stage files with `git add`
* (OPTIONAL) Show differences with `git diff`
* Commit files with `git commit -m "Commit message"`

---

## Branches

```console
$ git status
On branch master
```

`master` is the name of the main branch of the code. Ideally, it contains the state-of-the-art **working** code of the project.

![](master.png)
---

We now want to make a change to the code (a new feature, a change, a bugfix...)

We create a **branch**: we copy the current status of the repository.

![](branch.png)


---

We now proceed we the development of our new feature. We add commits to the new branch. The `master` branch will not be touched.

![](branch_commits.png)

---

When our feature/bug fix/whatever is complete, we can now **merge** it to the `master` branch.

![](https://wac-cdn.atlassian.com/dam/jcr:502e5e3e-73db-45cb-97b1-6149aab57759/06.svg?cdnVersion=kc)

---

### Creating a branch

```console
$ git checkout -b age
Switched to a new branch 'age'
```

We now make a change to `hello.txt`:

```text
Hello world!

My name is Matteo.

My age is 28.
```

We stage and commit it

```console
$ git add .

$ git commit -m "Age added"
[age 6bb7f97] Age added
 1 file changed, 3 insertions(+), 1 deletion(-)
```

---

## Merging
We are satisfied with our new feature. Now we want to merge it to the `master` branch.

We move our working directory to the master branch.

```console
$ git checkout master
Switched to branch 'master'
```

>Open the `hello.txt` file now. The age has disappeared!

---

To merge the chosen branch (`age`) into the active branch (`master`), we use the command

```console
$ git merge age
Updating d6615e3..6bb7f97
Fast-forward
 hello.txt | 4 +++-
 1 file changed, 3 insertions(+), 1 deletion(-)
```

Now check the content of the file `hello.txt`.

---

More complicated situations can arise

![](https://wac-cdn.atlassian.com/dam/jcr:86eba9ec-9391-45ea-800a-948cec1f2ed7/Branch-2.png?cdnVersion=kc)

The `master` branch evolves while you are working on a feature, or you are working on more than one feature at a time.

---

There might be conflicts when you want to `merge` your new feature

![](https://wac-cdn.atlassian.com/dam/jcr:83323200-3c57-4c29-9b7e-e67e98745427/Branch-1.png?cdnVersion=kc)

---

## Exercise

1. Create one branch called `position`, add a line like

    ```text
    I am a postdoc
    ```
    to `hello.txt` and commit it.
2. Move back to `master`
3. Create one branch called `nationality`, add a line like

    ```text
    I am Italian
    ```
    to `hello.txt` and commit it.

---

### Visualizing the repository graph

```console
$ git log --oneline --abbrev-commit --all --graph --decorate
* b9ecf79 (nationality) Nationality added
| * a6ba814 (position) Position added
|/
* 6bb7f97 (HEAD -> master, age) Age added
* d6615e3 Name added
* 0e19cef Initial commit
```

> There are tools that show this graph in a fancier way!

---
## Resolving conflicts

Let's merge our new features. Let's make sure we are in the `master` branch first.

Then let's merge `position`

```console
$ git merge position
Updating 6bb7f97..a6ba814
Fast-forward
 hello.txt | 4 +++-
 1 file changed, 3 insertions(+), 1 deletion(-)
```

---
Now let's merge `nationality`

```console
$ git merge nationality
Auto-merging hello.txt
CONFLICT (content): Merge conflict in hello.txt
Automatic merge failed; fix conflicts and then commit the result.
```

Mhmhm, we are modifying the same line of code, and git prevents us from making a mess. We have to fix the conflict manually.

---
Let's run `git status`

```console
$ git status
On branch master
You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Unmerged paths:
  (use "git add <file>..." to mark resolution)

        both modified:   hello.txt

no changes added to commit (use "git add" and/or "git commit -a")
```

---

Let's open `hello.txt`

```text
Hello world!

My name is Matteo.

My age is 28.

<<<<<<< HEAD
I am a postdoc.
=======
I am Italian.
>>>>>>> nationality
```

Git has modified the file with the information about the conflict.

---

We edit the file manually so that both lines are written

```text
Hello world!

My name is Matteo.

My age is 28.
I am a postdoc.
I am Italian.
```

and we commit the merge.

---
This is how it looks in the end

```console
$ git log --oneline --abbrev-commit --all --graph --decorate
*   417c199 (HEAD -> master) Merge fixed
|\
| * b9ecf79 (nationality) Added nationality
* | a6ba814 (position) Position
|/
* 6bb7f97 (age) Age added
* d6615e3 Name added
* 0e19cef Initial commit
```

---

Branching looks quite useless at this point. But it becomes extremely useful when:

* More people are working on the same project
* You are developing new features in parallel
* You are revising published code


So far, we have been working locally.

Git allows for remote repositories. 


 Let's now use a remote repository on [gitlab.utu.fi](https://gitlab.utu.fi).

Click on the Plus sign > New project

---

![](gitlab_1.png)

---
In our command line, we run the commands

```console
$ git remote add origin https://gitlab.utu.fi/UTUNAME/git_tutorial.git/
```
and then

```console
$ git push -u origin --all
```

You will be asked your login information.

Now you have a remote copy of the directory.

---

## Pulling and pushing

The local and remote repository are not automatically synched. You have to do it
using the `pull` and `push` commands. The workflow is:

```console
$ git pull
Already up to date.
```

We make a change to `hello.txt`.

```console
$ git commit -m "Some local change"
```

The remote repository is untouched

---
```console
$ git push
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 4 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 293 bytes | 293.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0)
To https://gitlab.utu.fi/matros/git_tutorial_test
   417c199..83e1e69  master -> master
```

And now the change is uploaded to the remote repository.
![](gitlab_3.png)

---

## Part 2: Remote repositories

---

## Recap
* `git add`: add files to staging area
* `git commit -m "Message"`: commit the staged files with message
* `git checkout -b branchname`: create a new branch
* `git checkout master`: switch to an existing branch
* `git merge branchname`: merges `branchname` into current branch
* `git pull`: get latest version from remote repository
* `git push`: copy your local repository to the remote one


---

## Collaborating

Repositories can be used for collaborating on a project. 

Branches and `git merge` become fundamental to allow everyone to work on different
features.

The merging part is the most critical one. One has to check that the new code works
as expected, that it doesn't break other parts of the code etc.

---?image=git.png&size=auto 90%

---

GitHub and GitLab provide tools for handling the merging process: **pull requests**
or **merge requests**.

We will now try this feature with a repository that I already created: https://gitlab.utu.fi/matros/git_tutorial_collab

---

## Cloning an existing repository
First, let's **clone** the repository (it means, we download it locally).

Move to a desired location in your computer (away from any `git` repository) and

```console
$ git clone https://gitlab.utu.fi/matros/git_tutorial_collab.git
Cloning into 'git_tutorial_collab'...
remote: Enumerating objects: 6, done.
remote: Counting objects: 100% (6/6), done.
remote: Compressing objects: 100% (4/4), done.
remote: Total 6 (delta 1), reused 0 (delta 0)
Unpacking objects: 100% (6/6), done.
```

---

We now have the entire repository locally:

```console
$ cd git_tutorial_collab
```

```console
$ git log
commit e1fda898d2c74b55e92efc5bc4f7ff4915fcff58 (HEAD -> master, origin/master, origin/HEAD)
Author: Matteo Rossi <matteo.rossi@utu.fi>
Date:   Mon Oct 8 18:15:15 2018 +0300

    Removed a broken link

commit 1764fbe821ebfae8a3acb1d21d52bac2f1ebfaee
Author: Matteo Rossi <matteo.rossi@utu.fi>
Date:   Mon Oct 8 18:12:13 2018 +0300

    Add README.md
```

---

## Exercise

There is a file [`README.md`](https://gitlab.utu.fi/matros/git_tutorial_collab/blob/master/README.md), that is rendered on Gitlab.

But there are English mistakes in the sentences!

Each one of us will now fix **three** sentences (chosen randomly).

---
* `git pull`
* Create a branch `yourname` 
* Fix `README.md` and commit the changes.
* Push the branch and look at the result on GitLab.
    ```
    git push --set-upstream origin matteo_rossi
    ```

> You should have permission to push. If not, please complain.

* Navigate to your branch on Gitlab and create a *Merge request*

---

*Merge requests* are not necessary: you could have merged your branch to `master`
and then pushed `master` to the remote repository.

---

## Part 3:
## Tests and documentation

---

These topics are not strictly related to `git`, but to the repository services like GitHub and GitLab.

They allow for hosting documentation, building code and running tests.

---

### Unit Tests

Test that all the units of your code (i.e. functions, classess) do what they are
expected to do.

<img src="https://uploads.toptal.io/blog/image/91302/toptal-blog-image-1434578005589-4e6897ec04cc0b3c7075b9b011ee915c.gif" width="400">

--- 

When you make changes to the code, you want to run unit tests to check that 
nothing broke.

With `git` and Gitlab, they can be run automatically and remotely
whenever you commit a code change.

Let's go to https://gitlab.utu.fi/matros/python_test_tutorial.

---

## Exercise
### Homework maybe

* Fork the `python_test_tutorial` repository
* Add a unnecessary function (e.g. `add`, `abs`, ...) to `unnecessarymath.py`, including a **docstring** and **tests**
* Commit to your repository and check that tests are passed.
* Create a pull request in `python_test_tutorial`.